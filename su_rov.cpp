#include "su_rov.h"

SU_ROV::SU_ROV(QObject *parent) : QObject(parent) {

    K_Protocol = new Qkx_coeffs(ConfigFile, KI);
    X_Protocol = new x_protocol(ConfigFile,XI,X);
    model_protocol = new ModelProtocol(ConfigFile, "model");
    model = new ROV_Model;
    time = new QTimer;
    X[104][0] = 0;
    X[84][0] = 0;
    k1 = 0, k2 = 0, k3 = 0, k4 = 0;
    time->start(10); //частота 100Гц
    connect(time, SIGNAL(timeout()), SLOT(tick()));
    mode=Automatiz;

}

void SU_ROV::tick() {
    //записываем в Х-ы принятые с пульта управляющие команды
    get_data_from_pult();
    //записываем текущие координаты НПА в протокол обмена с ПУ
    send_data_to_pult();

    runge(0.01);

    //передаем заданные оператором значения в СУ и запускаем ее работу
    Control_Marsh();
    Control_Kurs();
    Control_Kren();
    Control_Different();
    Control_Depth();
    //полученные управляющиен сигналы с контуров передаем в БФС ДРК
    BFS_DRK(X[49][0],0,0,X[9][0],X[89][0] );
    //Полученные в БФС ДРК напряжения на ВМА ДРК передаем в модель ТНПА
    model->tick(X[27][0],X[28][0],X[29][0],X[30][0],X[102][0], X[103][0], 0.01);
    //записываем текущие параметры модели в соответствующие X-ы
    get_data_from_model();

}

void SU_ROV::BFS_DRK(double Upsi, double Uteta, double Ugamma, double Ux, double Uheight){

    //ограничим входные задающие сигналы в бфс ДРК
    X[11][0] = saturation(Ux, K[11]);
    X[12][0] = saturation(Upsi,K[12]);
    X[13][0] = saturation(Uteta,K[13]);
    X[14][0] = saturation(Ugamma, K[14]);
    X[101][0] = saturation(Uheight, K[15]);

    //далее по структурной схеме БФС, вычисляем значения после первого сумматора
    X[15][0] = -X[11][0] - X[12][0]; //промежуточное значение для ВМА МВЛ (управление курсом и маршем)
    X[16][0] = X[11][0] + X[12][0]; //промежуточное значение для ВМА МНЛ (управление курсом и маршем)
    X[17][0] = X[11][0] - X[12][0]; //промежуточное значение для ВМА МВП (управление курсом и маршем)
    X[18][0] = -X[11][0] + X[12][0]; //промежуточное значение для ВМА МНП (управление курсом и маршем)

    //далее по структурной схеме БФС, вычисляем значения после второго сумматора
    X[19][0] = X[15][0] + X[13][0];
    X[20][0] = X[16][0] + X[13][0];
    X[21][0] = X[17][0] - X[13][0];
    X[22][0] = X[18][0] - X[13][0];

    //далее по структурной схеме БФС, вычисляем значения после третьего сумматора
    X[23][0] = X[19][0] + X[14][0];
    X[24][0] = X[20][0] + X[14][0];
    X[25][0] = X[21][0] + X[14][0];
    X[26][0] = X[22][0] + X[14][0];

    //ограничим и промасштабируем управляющие значения напряжений для ВМА
    X[27][0] = saturation(X[23][0],K[23])*K[27]; //управляющее напряжение на ВМА МВЛ
    X[28][0] = saturation(X[24][0],K[24])*K[28]; //управляющее напряжение на ВМА МНЛ
    X[29][0] = saturation(X[25][0],K[25])*K[29]; //управляющее напряжение на ВМА МВП
    X[30][0] = saturation(X[26][0],K[26])*K[30]; //управляющее напряжение на ВМА МНП

    X[102][0] = X[101][0]*K[31]; //управляющее напряжение на ВМА вертикальный 1
    X[103][0] = X[101][0]*K[32]; //управляющее напряжение на ВМА вертикальный 2
}




void SU_ROV::Control_Kurs(){

    if (mode==Ruchnoi) {
        X[40][0]=X[1][0];
        X[47][0]=X[40][0]*K[40];
        X[48][0]=X[47][0];
        X[49][0]=saturation(X[48][0],K[49]);
    }
    else if (mode==Automatiz) {
        X[41][0]=X[1][0];
        X[43][0]=X[41][0]-X[42][0];
        X[44][0]=X[43][0]*K[44];

        X[45][0]=X[50][0]*K[45];
        X[46][0]=X[44][0]-X[45][0]+X[41][0]*K[43];
        X[48][0]=X[46][0];
        X[49][0]=saturation(X[48][0],K[49]);
    }

}

void SU_ROV::Control_Marsh(){
    X[9][0]=K[1];
}

void SU_ROV::get_data_from_pult(){
    X[8][0]=model_protocol->rec_data.VX_dest;
    X[2][0]=model_protocol->rec_data.VY_dest;
    X[3][0]=model_protocol->rec_data.VZ_dest;
    X[4][0]=model_protocol->rec_data.DEPTH_dest;
    X[5][0]=model_protocol->rec_data.GAMMA_dest;
    X[7][0]=model_protocol->rec_data.TETA_dest;
    X[1][0] = model_protocol->rec_data.PSI_dest; //заданный курс
    mode = model_protocol->rec_data.mode;

}

void SU_ROV::send_data_to_pult()
{
    model_protocol->send_data.PSI = X[42][0];
    model_protocol->send_data.y = X[82][0];
    model_protocol->send_data.mode = mode;
}

void SU_ROV::Control_Kren(){

}
void SU_ROV::Control_Different(){

}
void SU_ROV::Control_Depth(){

    if (mode==Ruchnoi) {
        X[80][0]=X[4][0];
        X[87][0]=X[80][0]*K[80];
        X[88][0]=X[87][0];
        X[89][0]=saturation(X[88][0],K[89]);
    }
    else if (mode==Automatiz) {
        X[81][0]=X[4][0];
        X[83][0]=X[81][0]-X[82][0];
        X[84][0]=X[83][0]*K[84];

        X[106][0]=saturation(X[104][0],K[100]);
        X[105][0] = X[84][0]+ X[106][0];

        X[85][0]=X[90][0]*K[85];
        X[86][0]=X[105][0]-X[85][0]+X[81][0]*K[83];
        X[88][0]=X[86][0];
        X[89][0]=saturation(X[88][0],K[89]);
    }

}



void SU_ROV::runge(double step)
{
    k1 = 0, k2 = 0;
    Control_Depth();
    k1 =  X[104][0];
    k2 =  X[84][0];
    X[104][0] = k1+0.5*step*X[84][0];
    Control_Depth();
    k2 =  k2 + 2*X[84][0];
    X[104][0] = k1+0.5*step*X[84][0];
    Control_Depth();
    k2 =  k2 + 2*X[84][0];
    X[104][0] = k1+step*X[84][0];
    Control_Depth();
    X[104][0] = k1+(step/6)*(k2+X[84][0]);

}

void SU_ROV::get_data_from_model(){
    X[32][0]=model->Fx;
    X[33][0]=model->vx_global;
    X[34][0]=model->vx_local;
    X[35][0]=model->x_global;
    X[36][0]=model->y_global;
    X[37][0]=model->z_global;
    X[39][0]=model->Tetta_g;
    X[40][0]=model->Gamma_g;

    X[42][0]=model->Psi_g;
    X[50][0]=model->W_Psi_g;

    X[82][0]=model->height;
    X[90][0]= model->vy_local;

}

float SU_ROV::saturation(float input, float max) {
    if (fabs(input)>= max)
        return (sign(input)*max);
    else return input;
}


