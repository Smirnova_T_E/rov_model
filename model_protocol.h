#ifndef MODEL_PROTOCOL_H
#define MODEL_PROTOCOL_H

#include <QObject>
#include <QTimer>
#include <QUdpSocket>


#pragma pack(push,1)
//enum можно использовать для присвоения названий
enum SU_MODE {Ruchnoi, Automatiz};



//структура данных для отправки
struct SendStruct {
     float X;           ///---Координата X, м
     float Y;           ///---Координата Y, м (глубина)
     float y;           ///---Координата y, м (отстояние)
     float Z;           ///---Координата Z, м
     float VX;          ///---Скорость по X, м/с
     float VY;          ///---Скорость по Y, м/с
     float VZ;          ///---Скорость по Z, м/с
     float PSI;         ///---Угол курса, град (0...360˚)
     float TETA;        ///---Угол дифферента, град (-90˚...+90˚)
     float GAMMA;       ///---Угол крена, град (-90˚...+90˚)
     float Wx;      ///---Скорость поворота вокруг оси Xa, рад/с
     float Wy;      ///---Скорость поворота вокруг оси Ya, рад/с
     float Wz;      ///---Скорость поворота вокруг оси Za, рад/с
     SU_MODE mode;
     union { // Установка состояний локальных контуров
         uchar contours_state_raw;
         struct {
             uchar contours_state_march      : 1; // Маршевый (X)
             uchar contours_state_glubina    : 1; // Вертикальный (Y)
             uchar contours_state_reserve    : 1;
             uchar contours_state_yaw        : 1; // Курсовой
             uchar contours_state_pitch      : 1; // Дифферентный
             uchar contours_state_roll       : 1; // Креновый
         };
     };
     uint checksum;
    };
struct ReceiveStruct {          ///---Заданные параметры
     float PSI_dest;            ///---Заданный угол курса, град (0...360˚)
     float TETA_dest;           ///---Заданный угол дифферента, град (-90˚...+90˚)
     float GAMMA_dest;          ///---Заданный угол крена, град (-90˚...+90˚)
     float DEPTH_dest;          ///---Заданная глубина, м
     float VX_dest;             ///---Заданная скорость по X, м/с
     float VY_dest;             ///---Заданная скорость по Y, м/с
     float VZ_dest;             ///---Заданная скорость по Z, м/с
     SU_MODE mode;
     union { // Проверка состояний локальных контуров
         uchar contours_state_raw;
         struct {
             uchar contours_state_march      : 1; // Маршевый (X)
             uchar contours_state_glubina    : 1; // Вертикальный (Y)
             uchar contours_state_reserve    : 1;
             uchar contours_state_yaw        : 1; // Курсовой
             uchar contours_state_pitch      : 1; // Дифферентный
             uchar contours_state_roll       : 1; // Креновый
         };
     };
     uint checksum;
    };
#pragma pack (pop)

class ModelProtocol : public QObject {
    Q_OBJECT
public:
    ReceiveStruct rec_data; //структура для приема данных
    SendStruct send_data; //структура для отпарвки данных
    explicit ModelProtocol(const QString & config = "protocols.conf",
                           const QString & name = "pult", QObject *parent = 0);
private:
    QTimer *timer; //таймер для отправки данных с определенной частотой
    QUdpSocket *receiveSocket; //UDP-socket для приема данных
    QUdpSocket *sendSocket; //UDP-socket для отпарвки данных
    QHostAddress ip_receiver, ip_sender; //receiver - наше приложение
                                         //sender - модель ТНПА
    int port_receiver, port_sender; //номера портов приема и передачи
    float frequency; //частота обмена данными ТНПА с ПУ
    //функция вычисления контрольной суммы
    uint checksum_i(const void * data, int size);
    //проверка верна ли контрольная сумма
    bool validate(const ReceiveStruct & data) {
        return (data.checksum == checksum_i(&data, sizeof(data) - 4));
    }
    bool validate(const SendStruct & data) {
        return (data.checksum == checksum_i(&data, sizeof(data) - 4));
    }
    void aboutSend() {
        send_data.checksum = checksum_i(&send_data, sizeof(send_data) - 4);
    } //функция, которая вычисляет контроьную сумму и записывает
    //ее в переменную checksum структуры отправки

signals:

public slots:
    void sendData(); //слот для отправки данных, который соединим
    // с сигналом от таймера
    void receiveData(); //слот для приема данных, который соединим с
    //сигналом readyRead() сокета receiveSocket, который создается
    //при наличии принятых пакетов
};

#endif // MODEL_PROTOCOL_H
