#include "model_protocol.h"
#include "qpiconfig.h"

ModelProtocol::ModelProtocol(const QString & config , const QString & name ,
                             QObject *parent) : QObject(parent) {

    QPIConfig conf(config, QIODevice::ReadOnly);
    QPIConfig::Entry & e(conf.getValue(name));
    //прежде чем создать соединение по сети между пультом и
    //ТНПА необходимо задать ip адреса и порты для отправки и приема данных
    //эти значения считываем из config-файла, имя которого передается
    //в конструктор класса (принцип аналогичен config-файлам kx_pult'a)
    ip_sender = e.getValue("sender.ip").value(); //ip ПУ
    ip_receiver = e.getValue("receiver.ip").value(); //ip ROV_Model
    port_sender = e.getValue("sender.port", 0); //порт ПУ
    port_receiver = e.getValue("receiver.port", 0); //порт ROV_Model
    frequency = e.getValue("receiver.frequency", 20); //частота передачи данных
    //если в config-файле не указана частота передачи данных, то
    //по умолчанию в качестве этого значения принимается 20
    //(второй параметр в функции getValue())

    qDebug() << "Sender (ROV) ip:" << ip_sender << "port:" << port_sender;
    qDebug() << "Receiver (Pult) ip:" << ip_receiver << "port:" << port_receiver;
    qDebug() << "Frequency:" << frequency;

    timer = new QTimer(this);
    timer->start(1000/frequency); //запускаем таймер

    receiveSocket = new QUdpSocket(this); //создаем сокет на прием данных
    qDebug()<<receiveSocket->bind(ip_receiver, port_receiver);
    //слушаем данные приходящие на ip-адрес ip_receiver,
    //на порт port_receiver
    sendSocket = new QUdpSocket(this); //создаем сокет на передачу данных
    qDebug()<<"binded to ip:" << ip_sender <<"port:" << port_sender;
    //соединим сигнал таймера с функцией sendData, отсылающей данные
    connect(timer, SIGNAL(timeout()), SLOT(sendData()));
    //соединим сигнал сокета receiveSocket о том, что данные готовы для
    //чтения с функцией receiveData()
    connect(receiveSocket, SIGNAL(readyRead()),SLOT(receiveData()));

    send_data.mode=Ruchnoi;
}

uint ModelProtocol::checksum_i(const void * data, int size) { // function for checksum (uint)
    uint c = 0;
    for (int i = 0; i < size; ++i)
        c += ((const uchar*)data)[i];
    return ~(c + 1);
}

void ModelProtocol::sendData(){
    aboutSend();//считаем контрольную сумму и записываем ее в переменную
    //checksum структуры send_data (типа SendStruct)

    //Отсылаем структуру send_data на ip_sender на порт port_sender
    sendSocket->writeDatagram((char *)&send_data, sizeof(send_data),ip_sender, port_sender);
    qDebug() <<"send to ip:" << ip_sender << "port:" << port_sender;
}

void ModelProtocol::receiveData(){
    while(receiveSocket->hasPendingDatagrams()) {
        ReceiveStruct rec;// создаем локальную переменную для приема данных до проверки
        receiveSocket->readDatagram((char *)&rec, sizeof(rec)); //считываем данные

        if (!validate(rec)) {
            //Функция validate возвращает true - если контрольная сумма верна
            //и возвращает false, если это не так
            //в этой части функции контрольная сумма не верна
            qDebug() << "Checksum validate" << validate(rec);
            continue;
            //оператор continue выполняет пропуск оставшейся части кода
            //      тела цикла и переходит к следующей итерации цикла
        }
        rec_data = rec;//Если контрльная сумма верна, то записываем
        //принятые данные в структуру rec_data
    }
}
