#ifndef SU_ROV_H
#define SU_ROV_H

#include <QObject>


#include "kx_protocol.h"
#include "qkx_coeffs.h"
#include "model_protocol.h"
#include "rov_model.h"

extern double X[2000][2];
extern QVector<double> K;


const QString ConfigFile = "protocols.conf";
const QString XI = "xi";
const QString KI = "ki";

class SU_ROV : public QObject {
    Q_OBJECT
public:
    explicit SU_ROV(QObject *parent = 0);

signals:

public slots:
    void tick();
private:
    Qkx_coeffs * K_Protocol;
    x_protocol * X_Protocol;
    ModelProtocol *model_protocol;
    QTimer *time;
    ROV_Model *model;
    SU_MODE mode;

    void get_data_from_pult();
    void send_data_to_pult();
    void Control_Kurs();
    void Control_Marsh();
    void Control_Kren();
    void Control_Different();
    void Control_Depth();
    void Control_Depth2();

    double k1, k2, k3, k4;

    void runge(double step);

    void BFS_DRK(double Upsi, double Uteta, double Ugamma, double Ux, double Uheight);

    void get_data_from_model();

    int sign(float input){
        return (input>=0) ? 1 : -1;
    }
    float saturation(float input, float max);


};

#endif // SU_ROV_H
